import random
import string

def generate_fake_tag():
    return ''.join(random.choices(string.ascii_letters + string.digits, k=6)) + '.PV'
    
def generate_fake_value(min=0, max=1):
    return random.uniform(min, max)
    